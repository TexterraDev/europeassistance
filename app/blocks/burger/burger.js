{
  const burger = document.querySelector('.burger');
  const menu = document.querySelector('.content--mobile-nav');
  const header = document.querySelector('.header');
  const page = document.querySelector('body');
  const menuItems = document.querySelectorAll('.drop-out__item');

  const showMobileMenu = () => {
    page.classList.toggle('fixed');
    burger.classList.toggle('cross');
    menu.classList.toggle('content--opened');
    header.classList.toggle('header--menu-opened');
    

    switchers.forEach((element) => {
        element.classList.remove('drop-out__switcher--opened');
        element.parentElement.classList.remove('drop-out__item--pressed');
    });
    menuItems.forEach((element) => {
        element.classList.remove('drop-out__item--pressed')
      });
  }

  burger.addEventListener('click', showMobileMenu);

}