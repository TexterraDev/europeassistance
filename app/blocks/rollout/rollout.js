{
  const rollout = document.querySelector('.rollout');


  const showYears = () => {
    let years = Array.from(document.querySelectorAll('.milestone'));

    years.shift();
    
    years[0].classList.toggle('milestone--line');

    years.forEach((element, index) => {
      if (index > 0) {
        element.classList.toggle('milestone--hidden');
      }
    })
  }

  const rolloutSwitcher = () => {
      rollout.classList.toggle('rollout--pressed');
        if (rollout.firstElementChild.textContent === 'Развернуть') {
          rollout.firstElementChild.textContent = 'Свернуть'
        } else {
          rollout.firstElementChild.textContent = 'Развернуть'
        }
      }

  if (rollout) {
    rollout.addEventListener('click', () => {
      showYears();
      rolloutSwitcher();
    })
  }
}
