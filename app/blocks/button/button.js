const scrollButtons = document.querySelectorAll('.button-scroll');
const scrollForm = document.querySelector('#form');

const headerDesktop = 142;
const headerMobile = 60;


const getCoords = () => {
  let box = scrollForm.getBoundingClientRect();

  if (window.matchMedia('(min-width: 960px)').matches) {
    let position = (box.top + pageYOffset) - headerDesktop;
    return position
  
  } else {
    let position = (box.top + pageYOffset) - headerMobile;
    return position
  }
}

const getScrolled = () => {
  window.scroll(0, getCoords());
}


scrollButtons.forEach((element) => {
  element.addEventListener('click', getScrolled);
})

