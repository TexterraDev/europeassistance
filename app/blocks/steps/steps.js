const sliderSteps = new Swiper ('.steps__container', {
  slidesPerView: 2,
  spaceBetween: 20,
  loop: false,
  pagination: {
    el: '.steps__pagination',
    clickable: true,
    dynamicBullets: true,
  },
  navigation: {
    nextEl: '.steps__button--next',
    prevEl: '.steps__button--prev'
  },
  breakpoints: {
    320: {
      slidesPerView: 1
    },
    768: {
      slidesPerView: 2
    }
  }
});