const sliderDivisions = new Swiper ('.divisions__slider', {
  slidesPerView: 2,
  allowTouchMove: false,
  loop: false,
  slideActiveClass: 'swiper-slide-thumb-active',
  breakpoints: {
    320: {
      direction: 'vertical'
    },
    708: {
      direction: 'horizontal'
    },
  }
});

const sliderServices = new Swiper ('.servicesSlider__continer', {
  slidesPerView: 1,
  loop: false,
  allowTouchMove: false,
  autoHeight: true,
  thumbs: {
    swiper: sliderDivisions
  }
});