const sliderInformation = new Swiper ('.informationSlider__container', {
  slidesPerView: 3,
  spaceBetween: 50,
  loop: false,
  pagination: {
    el: '.informationSlider__pagination',
    clickable: true,
    dynamicBullets: true
  },
  navigation: {
    nextEl: '.informationSlider__button--next',
    prevEl: '.informationSlider__button--prev'
  },
  breakpoints: {
    320: {
      slidesPerView: 1,
      spaceBetween: 0,
    },
    600: {
      slidesPerView: 2,
      spaceBetween: 20,
    },
    768: {
      slidesPerView: 3,
      spaceBetween: 50,
    }
  }
});