{
  const postTexts = document.querySelectorAll('.post__text');
  const moreButtons = document.querySelectorAll('.post__more');

  const symbolsAmountChecker = () => {
    postTexts.forEach((element) => {
      if (element.textContent.length > 377) {
        element.classList.add('post__text--caped');
        element.nextElementSibling.classList.remove('post__more--hidden');
      }
    });
  }
  symbolsAmountChecker();

  const morePost = (evt) => {
    let arrow = document.createElement('span');
    arrow.classList.add('post__pointer');

    evt.currentTarget.previousElementSibling.classList.toggle('post__text--caped');
    if (evt.currentTarget.textContent === 'Читать весь отзыв') {

      evt.currentTarget.textContent = 'Скрыть';
      evt.currentTarget.appendChild(arrow);
      evt.currentTarget.firstElementChild.classList.add('post__pointer--reversed');

    } else {

      evt.currentTarget.textContent = 'Читать весь отзыв';
      evt.currentTarget.appendChild(arrow);
      evt.currentTarget.firstElementChild.classList.remove('post__pointer--reversed');
    }

    moreButtons.forEach((element) => {
      if (element !== evt.currentTarget) {
        let arrow = document.createElement('span');
        arrow.classList.add('post__pointer');
        
        element.previousElementSibling.classList.add('post__text--caped');
        element.textContent = 'Читать весь отзыв';
        element.appendChild(arrow);
        arrow.classList.add('post__pointer');
      }
    });
  }

  moreButtons.forEach((element) => {
    element.addEventListener('click', (evt) => {
      event.preventDefault();
      morePost(evt);
    })
  })
}