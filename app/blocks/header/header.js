{

  if (window.matchMedia('(min-width: 960px)').matches) {
    const navigation = document.querySelector('nav');
    const header = document.querySelector('.header');
    const content = document.querySelector('main');
    const dropout = document.querySelector('.drop-out');
    const overlay = document.querySelector('.overlay');
    const links = document.querySelectorAll('.header-links__link');
    
    if (overlay === null) {
      navigation.classList.add('shadow');
      window.addEventListener('scroll', () => {

        if (window.pageYOffset >= 1) {
          header.classList.add('header--scrolled');
          content.classList.add('main--scrolled');
          navigation.classList.remove('shadow');
        } else {
          header.classList.remove('header--scrolled');
          content.classList.remove('main--scrolled');
          navigation.classList.add('shadow');
        }
      });
    } else {
      dropout.classList.add('drop-out--white');
      links.forEach((element) => {
        element.classList.add('header-links__link--white');
      })
      window.addEventListener('scroll', () => {

        if (window.pageYOffset >= 1) {
          header.classList.add('header--scrolled');
          content.classList.add('main--scrolled');
        } else {
          header.classList.remove('header--scrolled');
          content.classList.remove('main--scrolled');
        }
      });
    }
  }

}



