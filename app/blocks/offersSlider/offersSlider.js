const sliderOffers = new Swiper ('.offersSlider__container', {
  slidesPerView: 3,
  spaceBetween: 50,
  loop: false,
  pagination: {
    el: '.offersSlider__pagination',
    clickable: true,
    dynamicBullets: true
  },
  navigation: {
    nextEl: '.offersSlider__button--next',
    prevEl: '.offersSlider__button--prev'
  },
  breakpoints: {
    320: {
      slidesPerView: 1,
      spaceBetween: 0,
    },
    600: {
      slidesPerView: 2,
      spaceBetween: 20,
    },
    768: {
      slidesPerView: 3,
      spaceBetween: 50,
    }
  }
});