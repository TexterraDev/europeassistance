const flagsSlider = new Swiper ('.flags', {
  slidesPerView: 5,
  centeredSlides: true,
  centeredSlidesBounds: true,
  allowTouchMove: false,
  loop: true,
  navigation: {
    nextEl: '.flags__button--next',
    prevEl: '.flags__button--prev'
  },
  breakpoints: {
    320: {
      slidesPerView: 1
    },
    600: {
      slidesPerView: 3
    },
    960: {
      slidesPerView: 5
    }
  }
});