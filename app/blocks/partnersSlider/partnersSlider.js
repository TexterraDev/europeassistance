const sliderPartners = new Swiper ('.partners', {
  slidesPerView: 6,
  spaceBetween: 20,
  loop: false,
  pagination: {
    el: '.partners__pagination',
    clickable: true,
    dynamicBullets: true
  },
  navigation: {
    nextEl: '.partners__button--next',
    prevEl: '.partners__button--prev'
  },
  breakpoints: {
    320: {
      slidesPerView: 1
    },
    360: {
      slidesPerView: 2
    },
    600: {
      slidesPerView: 3
    },
    768: {
      slidesPerView: 4
    },
    960: {
      slidesPerView: 5
    },
    988: {
      slidesPerView: 6
    }
  }
});