const sliderIndex = new Swiper ('.indexSlider__container', {
  slidesPerView: 1,

  loop: false,
  pagination: {
    el: '.indexSlider__pagination',
    clickable: true
  },
  navigation: {
    nextEl: '.indexSlider__button--next',
    prevEl: '.indexSlider__button--prev'
  }
});