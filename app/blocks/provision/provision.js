{
  const provisionButtons = document.querySelectorAll('.provision__switcher');
  let provisionPointer = document.createElement('span');

  const transformButton = (evt) => {
    provisionPointer.classList.add('provision__switcher-pointer');

    if (evt.currentTarget.firstElementChild.textContent === 'Развернуть') {
      evt.currentTarget.firstElementChild.textContent = 'Свернуть'
      evt.currentTarget.appendChild(provisionPointer);
    } else {
      evt.currentTarget.firstElementChild.textContent = 'Развернуть'
      evt.currentTarget.appendChild(provisionPointer);
    }
  }

  const switchAgile = (evt) => {
    evt.currentTarget.classList.toggle('provision__switcher--opened');
    evt.currentTarget.previousElementSibling.classList.toggle('provision__agile--opened');
    transformButton(evt);

    provisionButtons.forEach((element) => {
      if(evt.currentTarget !== element) {

      element.previousElementSibling.classList.remove('provision__agile--opened');
      element.firstElementChild.textContent = 'Развернуть';
      provisionPointer.classList.add('provision__switcher-pointer');
      }
    });
  }

  provisionButtons.forEach((element) => {
    element.addEventListener('click', switchAgile);
  })
}