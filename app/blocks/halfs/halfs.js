{
  const halfsSwitcher = document.querySelector('.halfs__switcher');
  const buttonText = document.querySelector('.halfs__button-text');
  const agile = document.querySelector('.halfs__agile');

  const agileMove = () => {
    let halfsPointer = document.createElement('span');
    halfsPointer.classList.add('halfs__pointer');

    halfsSwitcher.classList.toggle('halfs__switcher--opened');
    if (buttonText.textContent === 'Развернуть') {
      buttonText.textContent = 'Скрыть'
      buttonText.appendChild(halfsPointer);
    } else {
      buttonText.textContent = 'Развернуть'
      buttonText.appendChild(halfsPointer);
    }
    agile.classList.toggle('halfs__agile--opened');
  }
  if (halfsSwitcher) {
    halfsSwitcher.addEventListener('click', agileMove);
  }
}