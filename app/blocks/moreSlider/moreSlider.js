const sliderMore = new Swiper ('.moreSlider__slider', {
  slidesPerView: 3,
  spaceBetween: 20,
  loop: false,
  pagination: {
    el: '.moreSlider__pagination',
    clickable: true
  },
  navigation: {
    nextEl: '.moreSlider__button--next',
    prevEl: '.moreSlider__button--prev'
  },
  breakpoints: {
    320: {
      slidesPerView: 1
    },
    600: {
      slidesPerView: 2
    },
    900: {
      slidesPerView: 3
    }
  }
});