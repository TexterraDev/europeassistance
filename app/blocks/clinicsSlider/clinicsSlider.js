const clinicsSlider = new Swiper ('.clinics', {
  slidesPerView: 3,
  centeredSlides: true,
  spaceBetween: 20,
  centeredSlidesBounds: true,
  allowTouchMove: false,
  loop: true,
  navigation: {
    nextEl: '.clinics__button--next',
    prevEl: '.clinics__button--prev'
  },
  breakpoints: {
    320: {
      slidesPerView: 1
    },
    630: {
      slidesPerView: 3
    }
  }
});