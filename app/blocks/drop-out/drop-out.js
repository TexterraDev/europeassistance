const switchers = document.querySelectorAll('.drop-out__switcher');
const checkers = document.querySelectorAll('.drop-out__checker');
const bottomCheckers = document.querySelectorAll('.drop-out__bottom-checker');

{
  const elementsHider = (array) => {
    array.forEach((element) =>  {
      element.classList.remove('drop-out__checker--opened');
      element.nextElementSibling.classList.remove('drop-out__sub-menu--visible');
    });
  }

  if (window.matchMedia('(max-width: 960px)').matches) {

    const press = (evt) => {
      evt.target.classList.toggle('drop-out__switcher--opened');
      evt.target.parentElement.classList.toggle('drop-out__item--pressed');

      switchers.forEach((element) => {
        if (element !== evt.target) {
          element.classList.remove('drop-out__switcher--opened');
          element.parentElement.classList.remove('drop-out__item--pressed');
        }
      });
      elementsHider(checkers);
      elementsHider(bottomCheckers);
    }

    switchers.forEach((element) => {
      element.addEventListener('click', press);
    });
  }

  const openSubMenu = (evt) => {
    evt.target.classList.toggle('drop-out__checker--opened');
    evt.target.nextElementSibling.classList.toggle('drop-out__sub-menu--visible');

    if (evt.target.classList.contains('drop-out__checker')) {
        checkers.forEach((element) => {
          if (element !== evt.target) {
            element.classList.remove('drop-out__checker--opened');
            element.nextElementSibling.classList.remove('drop-out__sub-menu--visible');
          }
        elementsHider(bottomCheckers);
      });
    }
  }

      checkers.forEach((element) => {
    element.addEventListener('click', (evt) => {
      openSubMenu(evt);
    });
  });

  bottomCheckers.forEach((element) => {
    element.addEventListener('click', (evt) => {
      openSubMenu(evt);
    });
  });

}


$('.drop-out__link').parents('.drop-out__item').addClass('sublinks')