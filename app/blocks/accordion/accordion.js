// {
//   const container =  document.querySelector('.results');
//   const checkers = document.querySelectorAll('accordion__checker');
//   const visualize = (evt) => {
//     if(evt.target.classList.contains('accordion__question') 
//     || evt.target.classList.contains('accordion__category') 
//     || evt.target.classList.contains('accordion__heading')) {
//       evt.target.classList.toggle('accordion__category--opened');
//       evt.target.nextElementSibling.classList.toggle('accordion__hider');

//       let answers = document.querySelectorAll('.accordion > a');

//       answers.forEach((element) => {
//         if (evt.target !== element) {
//           element.classList.remove('accordion__category--opened');
//           element.nextElementSibling.classList.add('accordion__hider');
//         }
//       });
//     }
//     else if (evt.target.classList.contains('accordion__checker')) {
//       evt.target.parentElement.classList.toggle('accordion__category--opened');
//       evt.target.parentElement.nextElementSibling.classList.toggle('accordion__hider');

//       let answers = document.querySelectorAll('.accordion > a');

//       answers.forEach((element) => {
//         if (evt.target.parentElement !== element) {
//           element.classList.remove('accordion__category--opened');
//           element.nextElementSibling.classList.add('accordion__hider');
//         }
//       });
//     }
//   }
//   if (container) {
//     container.addEventListener('click', (evt) => {
//       visualize(evt);
//     });
//   }
// }
if($('.accordion__item').length){
  $('.accordion__item').parents('.accordion').find('.accordion__category').addClass('notempty');
}
if($('.accordion__answer').length){
  $('.accordion__answer').parents('.accordion').find('.accordion__question').addClass('notempty');
}
if($('.accordion__vacancy').length){
  $('.accordion__vacancy').parents('.accordion').find('.accordion__heading').addClass('notempty');
}

$('.accordion__category, .accordion__question, .accordion__heading').on('click', (e)=>{
  $(e.currentTarget).siblings('div').toggleClass('accordion__hider')
  $(e.currentTarget).toggleClass('accordion__category--opened')
})
