const cardsSlider = new Swiper ('.cardsSlider', {
  slidesPerView: 1,
  loop: true,
  allowTouchMove: false,
  autoHeight: true,
  navigation: {
    nextEl: '.flags__button--next',
    prevEl: '.flags__button--prev'
  }
});