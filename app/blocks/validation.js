{
  document.addEventListener("DOMContentLoaded", () => {

    const phone = document.querySelector('input[type=phone]');
    const inputs = Array.from(document.querySelectorAll('.form-input'));
    const filteredInputs = inputs.filter((element) => {
      return element.attributes.type.nodeValue !== 'phone';
    });

    filteredInputs.forEach(function (element) {
      element.setCustomValidity('Введённое значение должно быть больше 2 символов');
      element.addEventListener('change', () => {
          if (element.value.length === '' || element.value.length <= 2) {
            element.classList.add('form-input--invalid');
          } else {
            element.classList.remove('form-input--invalid');
            element.setCustomValidity('');
          }
        });
      });

      function mask() {
        var matrix = "+7 (___) ___ ____",
            i = 0,
            def = matrix.replace(/\D/g, ""),
            val = this.value.replace(/\D/g, "");
        if (def.length >= val.length) val = def;
        this.value = matrix.replace(/./g, function(a) {
            return /[_\d]/.test(a) && i < val.length ? val.charAt(i++) : i >= val.length ? "" : a
        });
      };

      if(phone) {
        phone.addEventListener("input", mask);

        phone.addEventListener('change', () => {
          if (phone.value.length < 17) {
            phone.classList.add('form-input--invalid');
            phone.value = '';
          } else {
            phone.classList.remove('form-input--invalid');
          }
        });
    }

  });
}