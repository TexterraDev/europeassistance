const doctorsSlider = new Swiper ('.doctorsSlider', {
  slidesPerView: 1,
  loop: true,
  allowTouchMove: false,
  autoHeight: true,
  navigation: {
    nextEl: '.clinics__button--next',
    prevEl: '.clinics__button--prev'
  }
});