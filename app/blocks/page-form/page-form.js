{
  document.addEventListener("DOMContentLoaded", () => {

    const pageInputs = document.querySelectorAll('.page-form__input');

    const passwords = document.querySelectorAll('.page-form__input[type="password"]');

    const simple = (tested) => {
      if (tested.validity.tooShort) {
        tested.classList.add('page-form__input--invalid');
      } else if (tested.validity.valueMissing) {
        tested.classList.add('page-form__input--invalid');
      } else  {
        tested.setCustomValidity('');
        tested.classList.remove('page-form__input--invalid');
      }
    }

    const complicated = () => {
      if (passwords[0].value === "" || passwords[1].value === "") { 
        passwords[0].setCustomValidity('Обязательное поле');
        passwords[1].setCustomValidity('Обязательное поле');
      }
      else if (passwords[0].value.length < 6 || passwords[1].value.length < 6) {
          passwords[0].classList.add('page-form__input--invalid');
          passwords[1].classList.add('page-form__input--invalid');
          passwords[0].setCustomValidity('Значение должно быть не короче 6 символов');
          passwords[1].setCustomValidity('Значение должно быть не короче 6 символов');
        }
      else if (passwords[0].value !== passwords[1].value) {
          passwords[0].classList.add('page-form__input--invalid');
          passwords[1].classList.add('page-form__input--invalid');
          passwords[0].setCustomValidity('Значения не совпадают!');
          passwords[1].setCustomValidity('Значения не совпадают!');
        } 
        else {
          passwords[0].classList.remove('page-form__input--invalid');
          passwords[1].classList.remove('page-form__input--invalid');
          passwords[0].setCustomValidity('');
          passwords[1].setCustomValidity('');
        }
    }

    if(pageInputs) {
      pageInputs.forEach((element) => {
        element.addEventListener('invalid', () => {
          simple(element);
          if (passwords.length >= 2) {
            complicated();
          }
        });
        element.addEventListener('input', () => {
          simple(element);
          if (passwords.length >= 2) {
            complicated();
          }
        });
      });
    }
  });
}